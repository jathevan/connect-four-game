# Connect Four Game
Text Console-based Connect Four game

Connect Four is a two-player connection board game, in which the players choose a color and then take turns dropping colored discs into a seven-column, six-row vertically suspended grid. The pieces fall straight down, occupying the lowest available space within the column. The objective of the game is to be the first to form a horizontal, vertical, or diagonal line of four of one's own discs. Connect Four is a solved game. The first player can always win by playing the right moves.

Make sure you've Node.js 14.0 version and yarn installed in your system

Here I used MVC architecture with SOLID principle & minmax alpha beta pruning algorithm

#Run the Game
1. Open terminal
2. Type `yarn compile` to build the game
3. Type `yarn start` to start the game


#Run the Tests
Type `yarn test`

